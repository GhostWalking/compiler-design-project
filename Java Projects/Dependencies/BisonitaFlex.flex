package jflex;

import java_cup.runtime.Symbol;
%%

%unicode
%class BisonitaLexer
%cup
%implements sym 

%line
%column
 
%{
private Symbol symbol(int sym) {
    return new Symbol(sym, yyline+1, yycolumn+1);
}
  
private Symbol symbol(int sym, Object val) {
   return new Symbol(sym, yyline+1, yycolumn+1, val);
}

private void error(String message) {
   System.out.println("Error at line "+ (yyline+1) + ", column " + (yycolumn+ 1)+ " : "+message);
}
%} 

LineEnd = [\r\n]|\r\n
Character = [^\r\n]
WhiteSpace = {LineEnd} | [ \t\f]
 
LineComment = "//" {Character}* {LineEnd}
CStyleComment = "/*" ~"*/" 
Comment = {LineComment} | {CStyleComment}

String = "\"" ~"\""
Identifier = [:jletter:][:jletterdigit:]*
Zero = 0
DecInt = [1-9][0-9]*
 
Integer = {Zero} | {DecInt}
%%
<YYINITIAL> {
   
   "." { return symbol(DOT); } 
 
   /* Arithmetic Operations */
   "-" { return symbol(SUB);}
   "+" { return symbol(ADD); }
   "*" { return symbol(MULT); }
   "/" { return symbol(DIV); }
   "**" { return symbol(POW); }
   "and" { return symbol(AND); }
   "or" { return symbol(OR); }
   "mod"  { return symbol(MOD); }
   "not" { return symbol(NOT); }
 
   "<" { return symbol(LT); }
   ">" { return symbol(GT); }
   "<=" { return symbol(LTEQ); }
   ">=" { return symbol(GTEQ); }
   ":=" { return symbol(DECL); } 
   "=" { return symbol(EQL); }
   ";" { return symbol(SEMI); }
   "," { return symbol(COMMA); }
   
   /* Keywords */
   "program" { return symbol(PROGRAM); }
   "endprogram" { return symbol(ENDPROGRAM); }
   "int"     { return symbol(INT); }
   "real"     { return symbol(REAL); }
   "string"		{ return symbol(STRING) }
   "if" { return symbol(IF); }
   "then" { return symbol(THEN); }
   "else" { return symbol(ELSE); }
   "endif" { return symbol(ENDIF); }
   "while"   { return symbol(WHILE); }
   "do"   { return symbol(DO); }
   "enddo"   { return symbol(ENDDO); }  
   "for"   { return symbol(FOR); }
   "to"   { return symbol(TO); }
   "endfor"   { return symbol(ENDFOR); }
   
   "read"    { return symbol(READ); }
   "clear" { return symbol(CLEAR); }
   "move"  { return symbol(MOVE); }
   "draw"    { return symbol(DRAW); }
   "write"   { return symbol(WRITE); }
   "set_color"    { return symbol(SETCOLOR); }
   "set_line"    { return symbol(SETLINE); }
   
   "false"  { return symbol(FALSE); }
   "true"  { return symbol(TRUE); }
   
 
   "(" { return symbol(LPAREN); }
   ")" { return symbol(RPAREN); }
   "[" { return symbol(LSQBKT); }
   "]" { return symbol(RSQBKT); }
   "{" { return symbol(LBRKT); }
   "}" { return symbol(RBRKT); }
   
   {Comment} {}
   {String}  { return symbol(STRING_LITERAL, new String(yytext())); }
   {Identifier} { return symbol(IDENTIFIER, yytext());}
   {Integer} { return symbol(INTEGER_LITERAL, new Integer(Integer.parseInt(yytext()))); }
   {WhiteSpace} { /* Ignore */ }
 

 
.|\n { System.out.println("ERROR");error(yytext());}
}